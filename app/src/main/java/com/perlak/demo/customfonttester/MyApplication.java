package com.perlak.demo.customfonttester;

import android.app.Application;

import com.perlak.demo.customfonttester.utils.FontHelper;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by mateusz on 12/6/14.
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(FontHelper.CUSTOM_FONT3, R.attr.fontPath);
    }
}
