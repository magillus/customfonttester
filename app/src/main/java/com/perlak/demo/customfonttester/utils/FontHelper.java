package com.perlak.demo.customfonttester.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mateusz on 12/4/14.
 */
public class FontHelper {
    public static final String CUSTOM_FONT = "fonts/UniversLTStd-CnObl.ttf";
    public static final String CUSTOM_FONT2 = "fonts/UniversLTStd-LightCn.ttf";
    public static final String CUSTOM_FONT3 = "fonts/UniversLTStd-BoldEx.ttf";
    private static FontHelper sInsance;
    private Context mContext;
    private Map<String, Typeface> mFontMap = new HashMap<String, Typeface>();

    private FontHelper(Context context) {
        this.mContext = context.getApplicationContext();
        mFontMap.put(CUSTOM_FONT, Typeface.createFromAsset(mContext.getAssets(), CUSTOM_FONT));
    }

    public static FontHelper getInstance(Context context) {
        if (sInsance == null) {
            sInsance = new FontHelper(context);
        }
        return sInsance;
    }

    public void setFont(TextView textView, String fontFile) {
        if (mFontMap.containsKey(fontFile)) {
            textView.setTypeface(mFontMap.get(fontFile));
        }
    }

}
