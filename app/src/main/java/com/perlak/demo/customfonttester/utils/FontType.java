package com.perlak.demo.customfonttester.utils;

/**
 * Created by mateusz on 12/16/14.
 */
public enum FontType {
    UNIVERSAL("fonts/UniversLTStd.ttf"),
    UNIVERSAL_BOLD("fonts/UniversLTStd-Bold.ttf"),
    UNIVERSAL_CN("fonts/UniversLTStd-Cn.ttf"),
    UNIVERSAL_LIGHT("fonts/UniversLTStd-Light.ttf");


    private String mType;

    FontType(String type) {
        mType = type;
    }

    public String getType() {
        return mType;
    }
}
