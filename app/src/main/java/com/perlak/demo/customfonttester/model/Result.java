package com.perlak.demo.customfonttester.model;

import java.io.Serializable;

/**
 * Created by Mateusz on 12/17/2014.
 */
public class Result implements Serializable {
    public Class testClazz;
    public double timeInMilis;
    public int count;
    public String name;
}
