package com.perlak.demo.customfonttester;

import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.perlak.demo.customfonttester.view.UniversalBoldTextView;


public class UniversalTextViewActivity extends BaseTextViewActivity {
    @Override
    protected void generateTextViews() {
        for (int i = 0; i < count; i++) {
            UniversalBoldTextView textView = new UniversalBoldTextView(getApplicationContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText("Custom font per each TextView font " + i);
            mContainer.addView(textView);
        }
    }

    @Override
    protected String getName() {
        return "Per font CustomTextView using FontHelper";
    }
}
