package com.perlak.demo.customfonttester;

import android.view.View;
import android.widget.TextView;


public class MyTextViewActivity extends BaseTextViewActivity {

    @Override
    protected void generateTextViews() {
        View view = null;
        for (int i = 0; i < count; i++) {
            if (i % 2 == 0) {
                view = getLayoutInflater().inflate(R.layout.bold_my_text_view, null, false);
            } else {
                view = getLayoutInflater().inflate(R.layout.light_my_text_view, null, false);
            }
            TextView textView = (TextView) view;
            textView.setTextColor(0xff000000);
            textView.setText("Calligraphy font " + i);
            mContainer.addView(textView);
        }
    }

    @Override
    protected String getName() {
        return "MyTextView wiht fontFamily attribute using FontHelper";
    }
}
