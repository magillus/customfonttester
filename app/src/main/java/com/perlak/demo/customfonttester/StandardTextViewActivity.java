package com.perlak.demo.customfonttester;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


public class StandardTextViewActivity extends BaseTextViewActivity {


    protected void generateTextViews() {
        for (int i = 0; i < count; i++) {
            TextView textView = new TextView(getApplicationContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText("Standard font " + i);
            mContainer.addView(textView);
        }
    }

    @Override
    protected String getName() {
        return "Standard TextView";
    }
}
