package com.perlak.demo.customfonttester.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.perlak.demo.customfonttester.utils.FontHelper;

/**
 * Created by mateusz on 12/16/14.
 */
public class UniversalBoldTextView extends TextView {
    public UniversalBoldTextView(Context context) {
        super(context);
        initialize(context);
    }

    public UniversalBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public UniversalBoldTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UniversalBoldTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context);
    }

    private void initialize(Context context) {
        FontHelper.getInstance(context).setFont(this, FontHelper.CUSTOM_FONT2);
    }
}
