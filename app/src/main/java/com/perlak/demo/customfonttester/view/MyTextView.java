package com.perlak.demo.customfonttester.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.perlak.demo.customfonttester.R;
import com.perlak.demo.customfonttester.utils.FontHelper;
import com.perlak.demo.customfonttester.utils.FontType;

/**
 * Created by mateusz on 12/16/14.
 */
public class MyTextView extends TextView {
    public MyTextView(Context context) {
        super(context);
        initialize(context, null);
    }

    public MyTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context, attrs);
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MyTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize(context, attrs);
    }

    private void initialize(Context context, AttributeSet attrs) {
        //get attribute font

        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs, R.styleable.MyTextView,
                0, 0);

        try {
            int fontString = a.getInteger(R.styleable.MyTextView_fontFamily, 0);
            FontType fontType = FontType.values()[fontString];

            FontHelper.getInstance(context).setFont(this, fontType.getType());
        } finally {
            a.recycle();
        }

    }
}
