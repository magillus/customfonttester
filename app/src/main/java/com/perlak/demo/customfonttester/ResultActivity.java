package com.perlak.demo.customfonttester;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.perlak.demo.customfonttester.model.Result;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


public class ResultActivity extends ActionBarActivity {
    private static final String RESULT_DATA = "RESULT_DATA";

    public static Intent createIntent(Context context, Collection<Result> results) {
        Intent intent = new Intent(context, ResultActivity.class);
        intent.putExtra(RESULT_DATA, results.toArray(new Result[0]));
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        ListView resultsList = (ListView) findViewById(R.id.result_list);
        // prepare adapter
        Object[] arr = (Object[]) getIntent().getSerializableExtra(RESULT_DATA);
        if (arr != null) {
            List<Result> results = new ArrayList<>();
            for (int i = 0; i < arr.length; i++) {
                results.add((Result) arr[i]);
            }
            ListAdapter adapter = new ResultAdatpter(this, results);
            resultsList.setAdapter(adapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_result, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ResultAdatpter extends ArrayAdapter<Result> {

        public ResultAdatpter(Context context, List<Result> objects) {
            super(context, R.layout.results_view, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = View.inflate(getApplicationContext(), R.layout.results_view, null);
            }
            Result item = getItem(position);
            if (item != null) {
                TextView name = (TextView) view.findViewById(R.id.class_name);
                name.setText(item.name);
                TextView result = (TextView) view.findViewById(R.id.result);
                result.setText(BaseTextViewActivity.getResultText(item.timeInMilis, item.count));
            }
            return view;

        }
    }
}
