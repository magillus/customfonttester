package com.perlak.demo.customfonttester;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perlak.demo.customfonttester.model.Result;


public abstract class BaseTextViewActivity extends ActionBarActivity {

    public static final String RESULT_DATA = "RESULT_DATA";
    public static final String AUTO_CLOSE_ARG = "AUTO_CLOSE_ARG";
    LinearLayout mContainer;
    TextView mResults;

    int count = 100;
    boolean autoClose = false;

    public static String getResultText(double timeTotal, int count) {
        StringBuilder sb = new StringBuilder();

        sb.append("time total: ");
        sb.append(String.valueOf(timeTotal) + " ms\n");
        sb.append("time avg:");
        sb.append(String.valueOf(timeTotal / count) + " ms\n");
        return sb.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        autoClose = (getIntent().getBooleanExtra(AUTO_CLOSE_ARG, false));
        //count = getResources().getInteger(R.integer.text_view_count);
        String sCount = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(SettingsActivity.PREF_TEXT_VIEW_COUNT, "10");
        count = Integer.valueOf(sCount);
        mContainer = (LinearLayout) findViewById(R.id.content);
        mResults = (TextView) findViewById(R.id.results);
        measureStats();
        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
    }

    protected int getLayoutResId() {
        return R.layout.activity_standard_text_view;
    }

    protected abstract void generateTextViews();

    private void measureStats() {
        long start = System.nanoTime();
        generateTextViews();
        long end = System.nanoTime();

        mContainer.requestLayout();
        double timeTotal = ((double) end - start) / 1000000;

        mResults.setText(getResultText(timeTotal, count));

        Result result = new Result();
        result.testClazz = this.getClass();
        result.name = getName();
        result.timeInMilis = timeTotal;
        result.count = count;
        Intent data = new Intent();
        data.putExtra(RESULT_DATA, result);
        setResult(MainActivity.TEST_REQUEST_CODE, data);
        if (autoClose) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    finish();
                }
            }, 200);
        }

    }

    protected abstract String getName();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_standard_text_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
