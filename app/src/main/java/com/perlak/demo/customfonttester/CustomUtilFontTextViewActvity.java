package com.perlak.demo.customfonttester;

import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perlak.demo.customfonttester.utils.FontHelper;


public class CustomUtilFontTextViewActvity extends BaseTextViewActivity {

    protected void generateTextViews() {
        for (int i = 0; i < count; i++) {
            TextView textView = new TextView(getApplicationContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText("CustomFont with Util font " + i);

            textView.setTextColor(0xff000000);
            FontHelper.getInstance(getApplicationContext()).setFont(textView, FontHelper.CUSTOM_FONT);
            mContainer.addView(textView);
        }
    }

    @Override
    protected String getName() {
        return "TextView with Typeface from FontHelper";
    }
}
