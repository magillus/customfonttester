package com.perlak.demo.customfonttester;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.perlak.demo.customfonttester.model.Result;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;


public class MainActivity extends ActionBarActivity {
    public static final int TEST_REQUEST_CODE = 34234;

    private Map<Class, List<Result>> resultMap = new HashMap<>();
    private List<TestWrapper> originalList = new ArrayList<>();
    private Queue<TestWrapper> testList = new ArrayDeque<>();

    private class TestWrapper {
        Class testClazz;
        int testCount = 1;

        public TestWrapper(Class testActivity, int testCount) {
            testClazz = testActivity;
            this.testCount = testCount;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).registerOnSharedPreferenceChangeListener(new SharedPreferences.OnSharedPreferenceChangeListener() {
            @Override
            public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
                if (key.equalsIgnoreCase(SettingsActivity.PREF_TEST_COUNT)) {
                    updateTestActivities();
                }
            }
        });
        updateTestActivities();
    }

    private void updateTestActivities() {
        String sCount = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(SettingsActivity.PREF_TEST_COUNT, "1");
        int testCount = Integer.valueOf(sCount);
        originalList.add(new TestWrapper(StandardTextViewActivity.class, testCount));
        originalList.add(new TestWrapper(UniversalTextViewActivity.class, testCount));
        originalList.add(new TestWrapper(CustomUtilFontTextViewActvity.class, testCount));
        originalList.add(new TestWrapper(CustomFontTextViewActvity.class, testCount));
        originalList.add(new TestWrapper(MyTextViewActivity.class, testCount));
        originalList.add(new TestWrapper(CalligraphyTextViewActivity.class, testCount));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == TEST_REQUEST_CODE) {
            Result result = (Result) data.getSerializableExtra(BaseTextViewActivity.RESULT_DATA);
            if (result != null) {
                if (resultMap.containsKey(result.testClazz)) {
                    resultMap.get(result.testClazz).add(result);
                } else {
                    List<Result> results = new ArrayList<>();
                    results.add(result);
                    resultMap.put(result.testClazz, results);
                }
            }
            startTestStep();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void onClickStandard(View view) {
        startActivity(StandardTextViewActivity.class);
    }

    private void startActivity(Class clazz) {
        startActivity(clazz, false);
    }

    private void startActivity(Class clazz, boolean test) {
        Intent intent = new Intent(getApplicationContext(), clazz);
        intent.putExtra(BaseTextViewActivity.AUTO_CLOSE_ARG, test);

        startActivityForResult(intent, TEST_REQUEST_CODE);
    }

    public void onClickCustom1(View view) {

        startActivity(CustomFontTextViewActvity.class);
    }

    public void onClickCustom2(View view) {
        startActivity(CustomUtilFontTextViewActvity.class);
    }

    public void onClickCalligraphy(View view) {
        startActivity(CalligraphyTextViewActivity.class);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onClickUniversal(View view) {
        startActivity(UniversalTextViewActivity.class);
    }

    public void onClickMyTextView(View view) {
        startActivity(MyTextViewActivity.class);
    }

    public void onClickRunAll(View view) {
        testList.addAll(originalList);
        startTestStep();
    }

    private void startTestStep() {
        if (testList.size() > 0) {
            TestWrapper testWrapper = testList.peek();
            if (testWrapper.testCount <= 1) testWrapper = testList.poll();
            testWrapper.testCount--;
            startActivity(testWrapper.testClazz, true);
        } else {
            // show results
            //aggregate results
            List<Result> resultAggregation = new ArrayList<>();
            for (Map.Entry<Class, List<Result>> entry : resultMap.entrySet()) {
                Result result = new Result();
                result.testClazz = entry.getValue().get(0).testClazz;
                result.name = entry.getValue().get(0).name;
                for (Result resultItem : entry.getValue()) {
                    result.count += resultItem.count;
                    result.timeInMilis += resultItem.timeInMilis;
                }
                resultAggregation.add(result);
            }
            startActivity(ResultActivity
                    .createIntent(this, resultAggregation));
            Toast.makeText(getApplicationContext(), "Tests completed:", Toast.LENGTH_SHORT).show();
        }
    }
}
