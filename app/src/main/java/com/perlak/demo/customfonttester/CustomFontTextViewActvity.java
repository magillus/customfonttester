package com.perlak.demo.customfonttester;

import android.graphics.Typeface;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.perlak.demo.customfonttester.utils.FontHelper;


public class CustomFontTextViewActvity extends BaseTextViewActivity {


    protected void generateTextViews() {
        for (int i = 0; i < count; i++) {
            TextView textView = new TextView(getApplicationContext());
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setText("Custom font per each TextView font " + i);

            textView.setTextColor(0xff000000);
            Typeface tf = Typeface.createFromAsset(getAssets(), FontHelper.CUSTOM_FONT);
            textView.setTypeface(tf);
            mContainer.addView(textView);
        }
    }

    @Override
    protected String getName() {
        return "TextView with Typeface load inline";
    }
}
