package com.perlak.demo.customfonttester;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by mateusz on 12/6/14.
 */
public class CalligraphyTextViewActivity extends BaseTextViewActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(new CalligraphyContextWrapper(newBase));
    }

    @Override
    protected void generateTextViews() {
        for (int i = 0; i < count; i++) {
            View view = getLayoutInflater().inflate(R.layout.calligraphy_text_view, null, false);
            TextView textView = (TextView) view;
            textView.setTextColor(0xff000000);
            textView.setText("Calligraphy font " + i);
            mContainer.addView(textView);
        }
    }

    @Override
    protected String getName() {
        return "Calligraphy library";
    }
}
